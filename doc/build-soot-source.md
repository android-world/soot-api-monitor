# 介绍

给文档主要记录如何利用eclipse下载soot源码并且编译，有了soot源码导入到eclipse中之后，可以方便的定位程序错误的位置，利于debug，同时也极大的方便使用者熟悉soot。国内下载soot可能需要Vpn。

# 下载、编译

## 1、运行eclipse

例如运行eclipse 4.3.0
	
## 2、下载soot源码

	File -> import -> team project set -> Next -> Enter the above URL -> Click Finish

该处的URL可以输入：https://raw.github.com/Sable/soot/develop/soot.psf。如果该URL失效，则该处可以选择不输入URL而是导入一个soot.psf文件，该soot.psf文件可按照如下方式写：

		<?xml version="1.0" encoding="UTF-8"?>
		<psf version="2.0">
		<provider id="org.eclipse.egit.core.GitProvider">
		<project reference="1.0,https://github.com/Sable/heros.git,develop,."/>
		<project reference="1.0,https://github.com/Sable/jasmin.git,develop,."/>
		<project reference="1.0,https://github.com/Sable/soot.git,develop,."/>
		</provider>
		</psf>

## 编译

在soot工程上名称上点击右键，选择build project，等待一段时间后，soot工程编译完成。

# 基于soot源码开发

可在soot工程的src目录下新建自己的包名，将自己的代码写入，运行自己代码的main方法便可以运行了。

# 参考链接

	https://github.com/Sable/soot/wiki/Building-Soot-with-Eclipse
	
	https://github.com/Sable/soot/wiki/Building-Soot-with-IntelliJ-IDEA

	
