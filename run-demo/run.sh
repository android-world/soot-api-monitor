#!/bin/sh

#rm old system system.img
rm -rf system.img

#copy the new system.img
cp ../out/system.img .

#run the emulator
./emulator -kernel kernel-qemu -system system.img -data userdata.img -ramdisk ramdisk.img
