package com.yuyifei.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;

public class Record {
	//record instrumented method
	public static ArrayList<String> instrumentedMethodList = new ArrayList<String>();
	
	public static void recordInstrumentedMethod(ArrayList<String> aList) {
		File file = new File("logs/record");
		Collection<String> lines = aList;
		
		try {
			FileUtils.writeLines(file, lines);
		} catch (IOException e) {
			System.err.println("write record to file failed.!!!");
			e.printStackTrace();
		}
	}
}
