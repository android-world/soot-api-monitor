package com.yuyifei.util;

import java.io.IOException;

public class ShellCommandHelp {
	public static void execCommand(String cmd) {
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			int processExitCode = p.waitFor();
			if (processExitCode != 0) {
				throw new RuntimeException("Something went wrong during exec cmd:" + cmd);
			}
			
			System.out.println("run the emulator success!");
			
		} catch (IOException | InterruptedException e) {
			System.err.println("Runtime.getRuntime().exec(cmd) failed!!!");
			e.printStackTrace();
		}
	}
}