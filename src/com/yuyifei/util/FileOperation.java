package com.yuyifei.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileOperation {
	/*
	 * create a Dir 
	 */
	public static void createDir(File newDir) {
		System.out.println("mkdir " + newDir.getName() + "...");
		try {
			FileUtils.forceMkdir(newDir);
		} catch (IOException e) {
			System.err.println("mkdir " + newDir.getName() + " failed !!!");
			e.printStackTrace();
		}
	}
	
	/*
	 * copy file from src to dest
	 */
	public static void copyFile(File src, File dest) {
		System.out.println("copy " + src.getAbsolutePath() + " to " + dest.getAbsolutePath() + "...");
		try {
			FileUtils.copyFile(src, dest);
		} catch (IOException e) {
			System.err.println("copy " + src.getAbsolutePath() + " to " + dest.getAbsolutePath() + " failed ...");
			e.printStackTrace();
		}
	}
	
	/*
	 * detele a file if it exist.
	 */
	public static void deleteFile(File file) {
		if (file.exists()) {
			file.delete();
			System.out.println("delete " + file.getName() + " ...");
			
			return;
		}
		
		System.out.println("warnning: delete " + file.getName() +" failed. file is no-exist");
	}
	
	/*
	 * delete a dir
	 */
	public static void deteleDir(File tmpDir) {
		if (tmpDir.exists() && tmpDir.isDirectory()) {
			System.out.println("detele " + tmpDir.getName() + " dir ...");
			try {
				FileUtils.deleteDirectory(tmpDir);
			} catch (IOException e) {
				System.err.println("delete " + tmpDir.getName() + " failed !!!");
				e.printStackTrace();
			}
		}
	}
}
