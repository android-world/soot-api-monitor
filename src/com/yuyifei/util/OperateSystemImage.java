package com.yuyifei.util;

import com.yuyifei.util.ShellCommandHelp;

public class OperateSystemImage {
	/*
	 * repack the image file use mkyaffs2image
	 */
	public static void repackImage() {
		String cmd = "./mkyaffs2image system out/system.img";
		ShellCommandHelp.execCommand(cmd);
	}
	
	/*
	 * open the image(system.img) file use unyaffs
	 */
	public static void openImage() {
		String cmd = "./openImage.sh";
		ShellCommandHelp.execCommand(cmd);
	}
}
