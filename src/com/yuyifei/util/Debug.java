package com.yuyifei.util;

import java.util.ArrayList;
import java.util.Iterator;

import com.yuyifei.instrument.SensitiveApi;

public class Debug {
	/*
	 * seconds:delay time. eg:seconds=3 --> delay 3s
	 */
	public static void delay(int seconds) {
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	/*
	 * print ArrayList for debug
	 */
	public static void printArrayList(ArrayList<String> aList) {
		Iterator<String> it = aList.iterator();
		while(it.hasNext()) {
			String i = it.next();
			System.out.println(i);
		}
	}
	
	public static void printSensitiveApi(SensitiveApi apiObject) {
		System.out.println("---> signature:" + apiObject.getSignature());
		System.out.println("---> className:" + apiObject.getClassName());
		System.out.println("---> method:" + apiObject.getMethod());
	}
}
