package com.yuyifei.instrument;

import java.io.File;

import com.yuyifei.util.FileOperation;
import com.yuyifei.util.OperateSystemImage;
import com.yuyifei.util.ShellCommandHelp;

public class Finish {
	public static void doFinish() {
		//delete system/framework/framework.jar ---- old framework.jar
		File oldFramework = new File("system/framework/framework.jar");
		FileOperation.deleteFile(oldFramework);
		
		//copy new framework.apk to system/framework, and modify its name to framework.jar
		File srcFrameworkJar = new File ("sootOutput/framework.apk");
		File destFrameworkJar = new File ("system/framework/framework.jar");
		FileOperation.copyFile(srcFrameworkJar, destFrameworkJar);
		
		//delete old system.img file
		File oldSystemImage = new File("system.img");
		FileOperation.deleteFile(oldSystemImage);
		
		//mkdir a new dir ---- out
		FileOperation.createDir(new File("out"));
		
		//generate the new system.img file
		OperateSystemImage.repackImage();
	}
	
	/*
	 * run the default emulator in run-demo dir
	 */
	public static void runEmulator() {
		String cmd = "run-demo/run.sh";
		ShellCommandHelp.execCommand(cmd);
	}
}
