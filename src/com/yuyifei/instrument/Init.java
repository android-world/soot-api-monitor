package com.yuyifei.instrument;

import java.io.File;

import com.yuyifei.util.FileOperation;
import com.yuyifei.util.OperateSystemImage;

public class Init {
	/*
	 * copy file to work dir
	 */
	public static void copyFile2Project() {
		//mkdir a new dir ---- tmp
		FileOperation.createDir(new File("tmp"));
		
		//copy system.img to project
		File srcSystemImage = new File(Main.config.getSystemImagePath());
		File destSystemImage = new File("system.img");
		FileOperation.copyFile(srcSystemImage, destSystemImage);
		
		//open the system.img file use unyaffs tool
		OperateSystemImage.openImage();
		
		//copy framework.jar to tmp dir
		File srcFrameworkJar = new File ("system/framework/framework.jar");
		File destFrameworkJar = new File ("tmp/framework.apk");
		FileOperation.copyFile(srcFrameworkJar, destFrameworkJar);
	}
	
	/*
	 * delete some  history file 
	 */
	public static void deleteHistoryFile() {
		//delete sootOutput dir
		FileOperation.deteleDir(new File("sootOutput"));
		
		//detele system dir
		FileOperation.deteleDir(new File("system"));
		
		//detele system.img
		FileOperation.deleteFile(new File("system.img"));
		
		//detele tmp dir
		FileOperation.deteleDir(new File("tmp"));
		
		//detele out dir
		FileOperation.deteleDir(new File("out"));
	}
}
