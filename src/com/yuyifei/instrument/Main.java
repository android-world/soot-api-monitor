package com.yuyifei.instrument;

import com.yuyifei.util.Record;

public class Main {
	public static Config config = null;
	public static Global global = null;
	
	public static void main (String[] args) {
		//new  a Config object instance
		config = Config.getInstance();
		
		//new a Global object instance
		global = Global.getInstance();
		
		//delete some history file
		Init.deleteHistoryFile();
		
		//copy some file to project; eg: system file 
		Init.copyFile2Project();
		
		//run the soot 
		String apkPath = "tmp/framework.apk";
		sootOption.runSoot(apkPath);
		
		//record instrumented methods
		Record.recordInstrumentedMethod(Record.instrumentedMethodList);
		
		//do finsh operation
		Finish.doFinish();
		
		System.out.println("program run finish ...");
		//run emulator
		//Finish.runEmulator();
	}
}
