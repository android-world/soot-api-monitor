package com.yuyifei.instrument;

public class SensitiveApi {
	private String signature;
	private String className;
	private String method;
	
	public String getSignature() {
		return signature;
	}
	public String getClassName() {
		return className;
	}
	public String getMethod() {
		return method;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}
