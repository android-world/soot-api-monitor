package com.yuyifei.instrument;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Config {
	private static Config config = null;
	
	private String androidJarsPath = null;
	private String systemImagePath = null;
	
	private Config () {}
	
	public static synchronized Config getInstance() {
		if (config == null) {
			config = new Config();
			
			Properties props = new Properties();
			try {
				props.load(new FileInputStream("resource/default_properties"));
			} catch (FileNotFoundException e) {
				System.err.println("resource/default_properties is non-existent !!!");
				e.printStackTrace();
			} catch (IOException e) {
				System.err.println("open resource/default_properties failed!!!");
				e.printStackTrace();
			}
			
			config.androidJarsPath = props.getProperty("android.jars");
			config.systemImagePath = props.getProperty("system_img.path");
		}
			
		return config;
	}
	
	public String getAndroidJarsPath() {
		return androidJarsPath;
	}

	public String getSystemImagePath() {
		return systemImagePath;
	}
}
