package com.yuyifei.instrument;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class Global {
	private static Global global = null;
	
	//sensitive api list
	private ArrayList<SensitiveApi> apiList;

	private Global () {}
	
	public static synchronized Global getInstance() {
		if (global == null) {
			global = new Global();
			
			//load defalut api collection
			global.apiList = loadApiCollection();
		}
		
		return global;
	}

	private static ArrayList<SensitiveApi> loadApiCollection() {
		
		ArrayList<SensitiveApi> apiList = new ArrayList<SensitiveApi>();
		
		File file = new File("resource/default_api_collection");
		
		List<String> list = null;
		try {
			list = FileUtils.readLines(file);
		} catch (IOException e) {
			System.err.println("read default api collection file failed!!!");
			e.printStackTrace();
		}
		
		if ( list.size()==0 ) {
			System.out.println("warnning: default api collection is null...");
			return apiList;
		}
		
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			String i = it.next();
			if ( i.startsWith("#") || i.isEmpty())
				continue;
			
			SensitiveApi apiObject = paserSensitiveApi(i);
			apiList.add(apiObject);
		}
		
		return apiList;
	}
	
	private static SensitiveApi paserSensitiveApi(String signature) {
		SensitiveApi apiObject =  new SensitiveApi();
	
		String[] str = signature.split(";");
		String className = str[0].substring(1).replace('/', '.');
		String method = str[1].substring(2);
		
		apiObject.setSignature(signature);
		apiObject.setClassName(className);
		apiObject.setMethod(method);
		//Debug.printSensitiveApi(apiObject);//debug it
		
		return apiObject;
	}
	
	public ArrayList<SensitiveApi> getApiList() {
		return apiList;
	}
}