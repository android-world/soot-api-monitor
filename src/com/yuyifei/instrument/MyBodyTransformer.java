package com.yuyifei.instrument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.yuyifei.util.Record;

import soot.Body;
import soot.BodyTransformer;
import soot.PatchingChain;
import soot.Unit;
import soot.jimple.IdentityStmt;

public class MyBodyTransformer extends BodyTransformer{
	
	@Override
	protected void internalTransform(Body body, String phaseName,
			Map<String, String> arg2) {
		
		String declaringClass = body.getMethod().getDeclaringClass().toString();
		if (declaringClass.contains("R$id")
				|| declaringClass.contains("R$attr")
				|| declaringClass.contains("R$layout")
				|| declaringClass.contains("R$style")
				|| declaringClass.contains("R$menu")
				|| declaringClass.contains("BuildConfig")
				|| declaringClass.contains("R$drawable")
				|| declaringClass.contains("R$string"))
			return;
		
		//if this api is a sensitive api, we will do something in it
		if ( isSensitiveApi(body) ) {
			monitorApi(body);
		}
	}
	
	private boolean isSensitiveApi(Body body) {
		boolean flag = false;
		String method = body.getMethod().getName();
		String declareClass = body.getMethod().getDeclaringClass().getName();
		
		ArrayList<SensitiveApi> apiList = Main.global.getApiList();//get seneitive api List
		
		for (int i=0; i<apiList.size(); ++i) {
			SensitiveApi apiObject = apiList.get(i);
			if ( declareClass.equals(apiObject.getClassName()) && method.equals(apiObject.getMethod()) ) {
				Record.instrumentedMethodList.add(body.getMethod().getSignature());
				//Debug.delay(3);//delay 3s
				
				flag = true;
			}
		}
		
		return flag;
	}

	private void monitorApi(Body body) {
		PatchingChain<Unit> units = body.getUnits();
		Iterator<Unit> i = units.snapshotIterator();
		
		//System.out.println(body.getMethod().getDeclaringClass());
		while (i.hasNext()) {
			Unit u = i.next();
			
			if (u instanceof IdentityStmt) {
				System.out.println("--------->"  + body.getMethod().toString());
				System.out.println(".........>" + u.toString());
				continue;
			}
			resolveMeasure(u, body);
			return;
		}
	}

	private void resolveMeasure(Unit u, Body body) 
	{
		String methodName =  body.getMethod().toString();
		String declaringClassName = body.getMethod().getDeclaringClass().toString();
		
		//InstrumentMethod.instrumentSystemOutPrintln(u, body, "跟踪调试信息 余奕飞44444：" + methodName);
		
		InstrumentMethod.instrumentSystemOutPrintln(u, body, "跟踪调试信息--->" 
																+ "declaringClassName:" + declaringClassName
																+ "---->"
																+ "methodName:" + methodName);
	}
}
