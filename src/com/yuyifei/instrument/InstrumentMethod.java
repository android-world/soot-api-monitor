package com.yuyifei.instrument;

import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.AssignStmt;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticFieldRef;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class InstrumentMethod 
{
	/*
	 * instrument System.out.println
	 */
	public static void  instrumentSystemOutPrintln(Unit u, Body body, String info)
	{
		PatchingChain<Unit> units = body.getUnits();
		Chain<Local> locals = body.getLocals();
		
        // Add locals, java.io.printStream tmpRef
        Local tmpRef = Jimple.v().newLocal("tmpRef", RefType.v("java.io.PrintStream"));
        locals.add(tmpRef);
        
        {
        	//get a static field ref
        	StaticFieldRef field = Jimple.v().newStaticFieldRef(Scene.v()
        			.getField("<java.lang.System: java.io.PrintStream out>").makeRef());
        	//create a newAssignStmt
        	AssignStmt assign = Jimple.v().newAssignStmt(tmpRef, field);
        	//instrument a assign statement
        	units.insertBefore(assign, u);
        }
		
        // insert "tmpRef.println(info)"
        {
            SootMethod toCall = Scene.v().getMethod("<java.io.PrintStream: void println(java.lang.String)>");
            System.out.println("----->" + toCall.getSignature());
            
            // create a invokeExpr
            VirtualInvokeExpr invokeExpr = Jimple.v().newVirtualInvokeExpr(tmpRef, 
														        		toCall.makeRef(),
														        		StringConstant.v(info));
            //create a invokeStmt
            InvokeStmt invokeStmt = Jimple.v().newInvokeStmt(invokeExpr);
            //instrument a invokeStmt
            units.insertBefore(invokeStmt, u);
        }      
	}
}
